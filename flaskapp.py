import os
from datetime import datetime
from flask import Flask, request, flash, url_for, redirect, \
     render_template, abort, send_from_directory

app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')

@app.route('/')
def home():
  return render_template('salv_home.html')

@app.route('/update1')
def update1():
  var='somestring'
  return var

@app.route('/update2')
def update2():
  var='somestring2'
  return var

@app.route('/update3')
def update3():
  var='somestring3'
  return var

if __name__ == '__main__':
    app.run()
