var risposta;
function update() {
	console.log($('form').serialize());
	$.ajax({
		url: '/update',
		data: $('form').serialize(),
		type: 'POST',
		success: function(response) {
			console.log(response);
			risposta=response;
			chart3();
		},
		error: function(error) {
			console.log('err',error);
		}
	});
}

function chart3() {
	console.log('chart3');
	if ($('#chart_3').size() != 1) {
		return;
	}

	toplot=[];
	for (j in risposta.timestamp){
		toplot.push([risposta.timestamp[j]*1000,risposta.var_value[j]]);}
	//tracking curves:

	var sin = [],
		cos = [];
	for (var i = 0; i < 14; i += 0.1) {
		sin.push([i, Math.sin(i)]);
		cos.push([i, Math.cos(i)]);
	}
	plot = $.plot($("#chart_3"), [{
		data: toplot,
		label: "risposta timestamp",
		lines: {
			lineWidth: 1,
		},
		shadowSize: 0
	}
	], {
		series: {
			lines: {
				show: true
			}
		},
		crosshair: {
			mode: "x"
		},
		xaxis:{
			mode: "time",
			timeformat: "%y/%m/%d"
		},
		grid: {
			hoverable: true,
			autoHighlight: false,
			tickColor: "#eee",
			borderColor: "#eee",
			borderWidth: 1
		},
	});

	var legends = $("#chart_3 .legendLabel");
	legends.each(function() {
		// fix the widths so they don't jump around
		$(this).css('width', $(this).width());
	});

	var updateLegendTimeout = null;
	var latestPosition = null;

	function updateLegend() {
		updateLegendTimeout = null;

		var pos = latestPosition;

		var axes = plot.getAxes();
		if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max || pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) return;

		var i, j, dataset = plot.getData();
		for (i = 0; i < dataset.length; ++i) {
			var series = dataset[i];

			// find the nearest points, x-wise
			for (j = 0; j < series.data.length; ++j)
				if (series.data[j][0] > pos.x) break;

			// now interpolate
			var y, p1 = series.data[j - 1],
				p2 = series.data[j];

			if (p1 == null) y = p2[1];
			else if (p2 == null) y = p1[1];
			else y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);

			legends.eq(i).text(series.label.replace(/=.*/, "= " + y.toFixed(2)));
		}
	}

	$("#chart_3").bind("plothover", function(event, pos, item) {
		latestPosition = pos;
		if (!updateLegendTimeout) updateLegendTimeout = setTimeout(updateLegend, 50);
	});
}
