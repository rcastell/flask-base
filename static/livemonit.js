$(function() {

	var toplot = [];
	console.log('tipolt ininziale',typeof(toplot));
	function update_data() {

		$.ajax({
			url: '/livepoll',
			data: $('form').serialize(),
			type: 'POST',
			success: function(response) {
				console.log(response);
				toplot.push([response.timestamp[0]*1000,response.var_value[0]]);
				console.log('tipo toplot',typeof(toplot));
				plot.setData([toplot]);
				console.log([toplot]);
				console.log([getRandomData()]);
				plot.draw();
				setTimeout(update_data, updateInterval);
			},
			error: function(error) {
				console.log('err',error);
			}
		});

	}
	// We use an inline data source in the example, usually data would
	// be fetched from a server

	var data = [],
		totalPoints = 300;

	function getRandomData() {

		if (data.length > 0)
			data = data.slice(1);

		// Do a random walk

		while (data.length < totalPoints) {

			var prev = data.length > 0 ? data[data.length - 1] : 50,
				y = prev + Math.random() * 10 - 5;

			if (y < 0) {
				y = 0;
			} else if (y > 100) {
				y = 100;
			}

			data.push(y);
		}

		// Zip the generated y values with the x values

		var res = [];
		for (var i = 0; i < data.length; ++i) {
			res.push([i, data[i]])
		}

		return res;
	}

	// Set up the control widget

	var updateInterval = 1200;
	$("#updateInterval").val(updateInterval).change(function () {
		var v = $(this).val();
		if (v && !isNaN(+v)) {
			updateInterval = +v;
			if (updateInterval < 1) {
				updateInterval = 1;
			} else if (updateInterval > 2000) {
				updateInterval = 2000;
			}
			$(this).val("" + updateInterval);
		}
	});

	var plot = $.plot("#chart_3", [ toplot ], {
		series: {
			shadowSize: 0	// Drawing is faster without shadows
		},
		yaxis: {
			max:10000
		},
		xaxis: {
			mode:"time"
		}
	});



	update_data();

});
