from setuptools import setup

setup(name='salv_home',
      version='1.0',
      description='A basic Flask app with static files',
      author='rcastell',
      author_email='rcastell@cern.ch',
     install_requires=['Flask>=0.10.1'],
     )
